import java.util.Arrays;

class Solution {
    public int maxArea(int h, int w, int[] horizontalCuts, int[] verticalCuts) {
        // Sort the arrays to get the maximum gap between cuts
        Arrays.sort(horizontalCuts);
        Arrays.sort(verticalCuts);
        
        // Consider the gaps between consecutive cuts in horizontal and vertical directions
        long maxHorizontalGap = Math.max(horizontalCuts[0], h - horizontalCuts[horizontalCuts.length - 1]);
        long maxVerticalGap = Math.max(verticalCuts[0], w - verticalCuts[verticalCuts.length - 1]);
        
        for (int i = 1; i < horizontalCuts.length; i++) {
            maxHorizontalGap = Math.max(maxHorizontalGap, horizontalCuts[i] - horizontalCuts[i - 1]);
        }
        
        for (int i = 1; i < verticalCuts.length; i++) {
            maxVerticalGap = Math.max(maxVerticalGap, verticalCuts[i] - verticalCuts[i - 1]);
        }
        
        // Return the maximum area modulo 10^9 + 7
        return (int) ((maxHorizontalGap * maxVerticalGap) % 1000000007);
    }
}
